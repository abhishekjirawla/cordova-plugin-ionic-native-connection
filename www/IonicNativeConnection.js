var exec = require('cordova/exec');

var PLUGIN_NAME = "IonicNativeConnection"; // This is just for code completion uses.

var IonicNativeConnection = function() {}; // This just makes it easier for us to export all of the functions at once.
// All of your plugin functions go below this. 
// Note: We are not passing any options in the [] block for this, so make sure you include the empty [] block.
IonicNativeConnection.addGeofence = function(onSuccess, onError, userId, jobId) {
   exec(onSuccess, onError, PLUGIN_NAME, "addGeofence", [userId, jobId]);
};

IonicNativeConnection.removeGeofence = function(onSuccess, onError) {
   exec(onSuccess, onError, PLUGIN_NAME, "removeGeofence", []);
};

module.exports = IonicNativeConnection;

// //Plug in to Cordova
// cordova.addConstructor(function() {

//    if (!window.Cordova) {
//        window.Cordova = cordova;
//    };

//    if(!window.plugins) window.plugins = {};
//    window.plugins.IonicNativeConnection = new IonicNativeConnection();

//    if (typeof module != 'undefined' && module.exports)
//     module.exports = IonicNativeConnection;
// });