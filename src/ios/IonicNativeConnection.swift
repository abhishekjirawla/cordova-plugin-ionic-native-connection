/*
* Notes: The @objc shows that this class & function should be exposed to Cordova.
*/
import CoreLocation
import UserNotifications
import SystemConfiguration

@objc(IonicNativeConnection) class IonicNativeConnection : CDVPlugin {
    
    lazy var geoNotificationManager = GeoFancing()
    
  @objc(addGeofence:) // Declare your function name.
  func addGeofence(command: CDVInvokedUrlCommand) { // write the function code.
    /* 
     * Always assume that the plugin will fail.
     * Even if in this example, it can't.
     */
    

    // Set the plugin result to fail.//805
    var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "The Plugin Failed");
    // Set the plugin result to succeed.
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "The plugin succeeded");
    // Send the function result back to Cordova.
    self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
    if let userID = command.arguments[0] as? String, let jobID = command.arguments[1] as? String {
        geoNotificationManager = GeoFancing()
        geoNotificationManager.requestPermissionNotifiation()
        geoNotificationManager.initLocationManager(str_id:jobID, str_userID: userID)
    }
    
  }

  @objc(removeGeofence:) // Declare your function name.
  func removeGeofence(command: CDVInvokedUrlCommand) { // write the function code.
    geoNotificationManager.removeGeofence()
  }
}
    //----------- =============== GEOFENCE-------========================= CLASS----------========
    //GEOFENCE  --------CLASS ------------------------------
class GeoFancing : NSObject ,CLLocationManagerDelegate, UNUserNotificationCenterDelegate ,WebApiRequestDelegate{

        let locationmanager:CLLocationManager = CLLocationManager()
        let web = Webservice()
        var global_jobID = ""
        var global_userID = ""

    //MARK:- Set GeoFencing Locationa and Start Monitoring ------//-----------
        //INIT LOCATION ----------//---------------------------
    func initLocationManager(str_id:String,str_userID:String) {
            self.locationmanager.delegate = self
            self.locationmanager.requestAlwaysAuthorization()
            self.locationmanager.distanceFilter = 100
            self.locationmanager.startUpdatingLocation() //START LOCAION MONITOR --------
            web.delegate = self
            
//Call API ----//-WHENEVER YOU WANT CALL THIS API  FUNCTION------//--------------------------------
            var dict_param = [String:String]()
            dict_param["user_id"] = str_userID
            dict_param["id"] = str_id
            global_userID = str_userID
            self.web.timeentryjob_list(dictassetsUpdates: dict_param)
        //------------------------------------//--------------------------------------------------
        }
        
    func removeGeofence() {
//REMOVE OLD ----------
for object in self.locationmanager.monitoredRegions {
let region = object
self.locationmanager.stopMonitoring(for: region)
}
}
        //------------------API RESPONSE ------------------------------
        //MARK:- TimeENTRY Update response
        func responseTimeEntry(responseObj: NSDictionary) {
            let responseAllKey : NSArray = responseObj.allKeys as NSArray
            if responseAllKey.contains("status")
            {
                if let status = responseObj.value(forKey: "status") as? String {
                    
                    if status == "success" {
                        if let  result  = responseObj.value(forKey: "result") as? NSArray {
                            
                            if let dict = result.object(at: 0) as? NSDictionary {
                                
                                if let jobID = dict.value(forKey: "id") as? String ,let latitude =  dict.value(forKey: "latitude") as? String , let longitude =  dict.value(forKey: "longitude") as? String  {
                                    
                                    print("Called Success!!")
                                    self.global_jobID = jobID
                                    self.addGeofence(jobId: jobID, lat: Double("\(latitude)")!, long: Double("\(longitude)")!)
                                }
                            }
                        }
                        
                    }
                }
            }
            else {
                if let error = responseObj.value(forKey: "result") as? String {
                    print(error)
                }
            }
        }

        
        //MARK:- EXIT Region API RESPONSE response
        func responseExitRegion(responseObj: NSDictionary) {
            let responseAllKey : NSArray = responseObj.allKeys as NSArray
            if responseAllKey.contains("success")
            {
                //Getting response --------if success------
                print("Called Success!!")

            }
            else {
                if let error = responseObj.value(forKey: "result") as? String {
                    print(error)
                }
            }
        }

        //MARK:- ENTER REGION API response
        func responseEnterRegion(responseObj: NSDictionary) {
            let responseAllKey : NSArray = responseObj.allKeys as NSArray
            if responseAllKey.contains("success")
            {
                //Getting response --------if success------
                print("Called Success!!")
            }
            else {
                if let error = responseObj.value(forKey: "result") as? String {
                    print(error)
                }
            }
        }

        
        //MARK:- START GEOFENCE REGION MONITO-----------------//--------
        func addGeofence(jobId:String,lat:Double,long:Double) {//PASS ID AND LOCATION LAT,LONG START GEOFENCE 23.0669835,72.5618985
            
            let date = Date()
let monthString = date.month
print(monthString)
UserDefaults.standard.set(monthString, forKey: "date")
UserDefaults.standard.synchronize()
            
            //REMOVE OLD ----------
            for object in self.locationmanager.monitoredRegions {
                let region = object
                self.locationmanager.stopMonitoring(for: region)
            }
            
            let geofenc:CLCircularRegion = CLCircularRegion(center: CLLocationCoordinate2DMake(lat,long), radius: 805, identifier: "Job")
            self.locationmanager.startMonitoring(for:geofenc)
        }
        
        //MARK:- Get Updated Location --------//---------
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            for curentlocation in locations {
                print("\(String(describing: index)): \(curentlocation)")
            }
        }
        //MARK:- Location Error------------//------------
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("\(error.localizedDescription)")
        }
        
        //MARK:- Trigger called While Enter Region Which We have set for Geofenc -------//-----------
        func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
            let date = Date()
            let monthString = date.month
            let isMatch = self.checkWithCurrentDate(str_date: monthString)
            if !isMatch{
                self.removeGeofence()
                return
            }
            print("Enter: \(region.identifier)")
            if #available(iOS 10.0, *) {
                self.postnotification(eventtitle: "in") //CALL LOCAL NOTIFICATION WHILE ENTER SET REGION -IN
           //===============================================
                var dict_param = [String:String]() //CHANGE PARAM KEY AND VALUE
                dict_param["user_id"] = global_userID
                dict_param["action"] = "in"
                self.web.enterRegion(dictassetsUpdates: dict_param)
        //==================================================
            } else {
                // Fallback on earlier versions
            }
        }
        
        //MARK:- Trigger called While Exit Region Which We have set for Geofenc -------//-----------

        func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
            let date = Date()
            let monthString = date.month
            let isMatch = self.checkWithCurrentDate(str_date: monthString)
            if !isMatch{
                self.removeGeofence()
                return
            }
            print("Exited+s: \(region.identifier)")
            if #available(iOS 10.0, *) {
                self.postnotification(eventtitle: "out") //CALL LOCAL NOTIFICATION EXIT SET REGION ---OUT
                
            //===============================================
                var dict_param = [String:String]() //CHANGE PARAM KEY AND VALUE
                dict_param["user_id"] = global_userID
                dict_param["action"] = "out"
                self.web.enterRegion(dictassetsUpdates: dict_param)
        //==================================================

            } else {
                // Fallback on earlier versions
            }

        }
    
    //MARK:- Check Date is Current OR Different
func checkWithCurrentDate(str_date:String)-> Bool {
if let date_str = UserDefaults.standard.object(forKey: "date") as? String {
if str_date != date_str {
UserDefaults.standard.set(date_str, forKey: "date")
UserDefaults.standard.synchronize()
//Call REMOVE GeoFence Function===========================//==========
print("Call here remove geofenc fun!!")
    
    return false
}
else{
    return true
    }
}
    return false
}
        
    //-----------------------------------------------------------------------------------
        //SET REDION ID  --------------------SET BASE ON ID NEED TO MORE CHECK-----------------------
       /* func getMonitoredRegion(id: String) -> CLRegion? {
            for object in locationManager.monitoredRegions {
                let region = object
                if (region.identifier == id) {
                    return region
                }
            }
            return nil
        }

        //REMOVE REDION ID  -------------------REMOVE BASE ON ID NEED TO MORE------------------------
        func removeGeoNotification(id: String) {
            let region = getMonitoredRegion(id: id)
            if (region != nil) {
                locationManager.stopMonitoringForRegion(region!)
            }
        }*/
    //-----------------------------------------------------------------------------------
        
        
        //MARK:- Request For Local Notification -------//-------------
        func requestPermissionNotifiation() {
            let application = UIApplication.shared
            if #available(iOS 10.0, *){
                
                UNUserNotificationCenter.current().delegate = self
                
                let authorization: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(options: authorization) { (isauthorisation, error) in
                    if (error != nil) {
                        print(error!)
                    }
                    else{
                        if (isauthorisation) {
                            print(isauthorisation)
                            NotificationCenter.default.post(Notification(name: Notification.Name("AUTHORIZED")))
                        }
                    }
                }
            }
        }
        
        //MARK:- Post local Notification ----------------//---------------
        @available(iOS 10.0, *)
        func postnotification(eventtitle:String)  {
                let center = UNUserNotificationCenter.current()
                let content = UNMutableNotificationContent()
            
                content.title = "H&H Painting"
                if eventtitle == "in" {
                    content.body = "You are " + "\(eventtitle)" + " the job radius"
                }
                else {
                    content.body = "You are " + "\(eventtitle)" + " of the job radius"
                }
                
                content.sound = .default()
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let notificationRequest:UNNotificationRequest = UNNotificationRequest(identifier: "Region", content: content, trigger: trigger)
                center.add(notificationRequest) { (error) in
                    if let error = error {
                        print(error)
                    }
                    else {
                        print("Add")
                    }
                }

        }
        
    }



//-----------------------------API CALLING DELEGATE CLASS-------------------
@objc protocol WebApiRequestDelegate {

    @objc optional func responseTimeEntry(responseObj: NSDictionary) -> Void
    @objc optional func responseEnterRegion(responseObj: NSDictionary) -> Void
    @objc optional func responseExitRegion(responseObj: NSDictionary) -> Void

}

class Webservice: NSObject {

    var delegate: WebApiRequestDelegate?
    let APIBASEURL    = "https://hh.spark-staging.dev/api/"
    let timeEntry     = "time-entry/job-list"
    let timeEntrysaveradius     = "time-entry/save-radius-time-entry"

        let headers = [
        "Content-Type": "application/json",
        "Auth-Token": "b2JaWGIwWkRHT2cvZk1helV0VklDTG1GeXhKZkpBS3oxRVJUWmE0Wi9GRT0"
        ]

    ////MARK:- time ENTRY JOB ---------------------
    func timeentryjob_list(dictassetsUpdates:[String:String])
    {
        // print(strjason)
        let parameters = dictassetsUpdates
        
        print(parameters)
        let json: [String: Any] = parameters

        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        //.......................................................//..................
        let str_Url:String = "\(APIBASEURL)\(timeEntry)"
        
        print("==========================")
        print(str_Url);
        print("==========================")
        var request: URLRequest = URLRequest(url: NSURL(string: str_Url)! as URL)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.allHTTPHeaderFields = headers
        request.timeoutInterval = 5000000000000000000000000.0

        let dataTask = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            
            if (response != nil) {
                
                if let httpResponse : HTTPURLResponse = response as? HTTPURLResponse
                {
                    let responseCode : Int = (httpResponse.statusCode)
                     print(responseCode)
                    if data != nil {
                        
                        if responseCode == 200 {
                            let emptyDict = NSDictionary()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseTimeEntry?(responseObj: emptyDict)
                            }
                        }
                        do {
                            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
                              print(json)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseTimeEntry?(responseObj: json)
                            }
                        } catch _ as NSError {
                            
                            let responseData = String(data: data!, encoding: String.Encoding.utf8)
                            print(responseData!)
                            
                            let emptyDict = NSDictionary()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseTimeEntry?(responseObj: emptyDict)
                            }
                        }
                    } else {
                        let emptyDict = NSDictionary()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                            self.delegate?.responseTimeEntry?(responseObj: emptyDict)
                        }
                    }
                }
            }
            else{

            }
        })
        dataTask.resume()
    }
    
    
    // MARK:- ENTER REGION  == IN
    func enterRegion(dictassetsUpdates:[String:String])
    {
        // print(strjason)
        let parameters = dictassetsUpdates
        let json: [String: Any] = parameters

        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        print(parameters)
        
        let boundary = generateBoundaryString()
        //.......................................................//..................
        let str_Url:String = "\(APIBASEURL)\(timeEntrysaveradius)" //<<------------------CHANGE ENDPONT
        
        print("==========================")
        print(str_Url);
        print("==========================")
        var request: URLRequest = URLRequest(url: NSURL(string: str_Url)! as URL)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.allHTTPHeaderFields = headers
        request.timeoutInterval = 5000000000000000000000000.0

        let dataTask = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            
            if (response != nil) {
                
                if let httpResponse : HTTPURLResponse = response as? HTTPURLResponse
                {
                    let responseCode : Int = (httpResponse.statusCode)
                     print(responseCode)
                    if data != nil {
                        let responseData = String(data: data!, encoding: String.Encoding.utf8)
                        print(responseData!)

                        if  responseCode == 200 {
                            let emptyDict = NSDictionary()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseEnterRegion?(responseObj: emptyDict)
                            }
                        }
                       /* do {
                            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
                              print(json)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseEnterRegion?(responseObj: json)
                            }
                        } catch _ as NSError {
                            
                            let responseData = String(data: data!, encoding: String.Encoding.utf8)
                            print(responseData!)
                            
                            let emptyDict = NSDictionary()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseEnterRegion?(responseObj: emptyDict)
                            }
                        }*/
                    } else {
                        let emptyDict = NSDictionary()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                            self.delegate?.responseEnterRegion?(responseObj: emptyDict)
                        }
                    }
                }
            }
            else{

            }
        })
        dataTask.resume()
    }
    
    //MRK:- EXIT REGION - OUT ===== Call API
    func exitRegion(dictassetsUpdates:[String:String])
    {
        // print(strjason)
        let parameters = dictassetsUpdates
        let json: [String: Any] = parameters

        let jsonData = try? JSONSerialization.data(withJSONObject: json)


        print(jsonData!)
        
        let boundary = generateBoundaryString()
        //.......................................................//..................
        let str_Url:String = "\(APIBASEURL)\(timeEntrysaveradius)"  //<<------------------CHANGE ENDPONT
        
        print("==========================")
        print(str_Url);
        print("==========================")
        var request: URLRequest = URLRequest(url: NSURL(string: str_Url)! as URL)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.allHTTPHeaderFields = headers

        request.timeoutInterval = 5000000000000000000000000.0

        let dataTask = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            
            if (response != nil) {
                
                if let httpResponse : HTTPURLResponse = response as? HTTPURLResponse
                {
                    let responseCode : Int = (httpResponse.statusCode)
                     print(responseCode)
                    if data != nil {
                        
                        do {
                            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
                              print(json)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseExitRegion?(responseObj: json)
                            }
                        } catch _ as NSError {
                            
                            let responseData = String(data: data!, encoding: String.Encoding.utf8)
                            print(responseData!)
                            
                            let emptyDict = NSDictionary()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.delegate?.responseExitRegion?(responseObj: emptyDict)
                            }
                        }
                    } else {
                        let emptyDict = NSDictionary()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                            self.delegate?.responseExitRegion?(responseObj: emptyDict)
                        }
                    }
                }
            }
            else{

            }
        })
        dataTask.resume()
    }
    
    //----------------------//----------------------
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}
//---------------------//-------------------------------------------
class Constants : NSObject {
    
    //--------------------BETA---------------------------------------------------------

    //MARK:- SQLite Database & Table
    
    // MARK: - AlertView Controller
    
    static func showAlertTitle(_ titleStr: String,messageStr: String , viewController : UIViewController) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true) {
        }
    }
    
    //MARK: - SDWebImageOptions Related Constants
    
    //MARK:- MAP Related Constants
    static let kMapDistance : Double = 1000
   static let SessionID = NSUUID().uuidString

}
extension Date {
var month: String {
let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "dd"
return dateFormatter.string(from: self)
}
}
