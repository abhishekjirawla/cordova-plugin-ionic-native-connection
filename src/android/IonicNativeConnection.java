/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package org.apache.cordova.ionicnativeconnection;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.icu.util.Calendar;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.spark.HHPainting.BuildConfig;
import com.spark.HHPainting.GeofenceBroadcastReceiver;
import com.spark.HHPainting.PrefUtils;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressLint("NewApi")
public class IonicNativeConnection extends CordovaPlugin {

    private static final String TAG = "HSGeoPlugin";

    private final static int GEOFENCE_RADIUS_IN_METERS = 805;
    private final static long GEOFENCE_EXPIRATION_IN_MILLISECONDS = Geofence.NEVER_EXPIRE;

    private static final String URL = "https://hh.spark-staging.dev/api/";
    private static final String TOKEN = "b2JaWGIwWkRHT2cvZk1helV0VklDTG1GeXhKZkpBS3oxRVJUWmE0Wi9GRT0";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private Activity activity;
    private CallbackContext callCtx;
    private double latitude;
    private double longitude;
    private GeofencingClient mGeofencingClient;
    private List<Geofence> mGeofenceList;
    private PendingIntent mGeofencePendingIntent;
    private JSONArray executeArgs;
    private String geofenceCreateDate;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getStringExtra("status");
            Log.d(TAG, "Got Geofence status: " + status);
            if (status != null) {
                if (status.equals("exit")) {
                    exitGeofenceAPI();
                } else if (status.equals("reenter")) {
                    reenterGeofenceAPI();
                }
            }
        }
    };

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        activity = cordova.getActivity();

        mGeofenceList = new ArrayList<>();
        mGeofencePendingIntent = null;
        mGeofencingClient = LocationServices.getGeofencingClient(activity);

        LocalBroadcastManager.getInstance(activity).registerReceiver(mMessageReceiver,
                new IntentFilter("geofenceStatus"));
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArry of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into JavaScript.
     * @return True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try {
            callCtx = callbackContext;
            executeArgs = args;

            Log.d(TAG, "execute: action " + action);
            Log.d(TAG, "execute: args " + executeArgs);

            if (executeArgs != null && executeArgs.length() > 0) {
                if (action.equals("addGeofence")) {
                    addGeofenceAPI();
                }
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            callbackContext.error("exception occurred: " + e.toString());
            return false;
        }
    }

    void addGeofenceAPI() {
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", executeArgs.getString(0));
            params.put("id", executeArgs.getString(1));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(JSON, params.toString());

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Auth-Token", TOKEN)
                .url(URL + "time-entry/job-list/")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "onFailure: api failed" + e.getLocalizedMessage());
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    //response.body() can be consumed only once so use only jsonData
                    String jsonData = response.body().string();
                    Log.d(TAG, "onResponse: " + jsonData);

                    JSONObject jsonObj = new JSONObject(jsonData);
                    String status = jsonObj.getString("status");
                    if (status != null && status.equals("success")) {
                        JSONArray jsonRes = jsonObj.getJSONArray("result");
                        JSONObject obj = jsonRes.getJSONObject(0);

                        latitude = Double.parseDouble(obj.getString("latitude"));
                        longitude = Double.parseDouble(obj.getString("longitude"));

                        removeGeofence();
                        addGeoFence();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        activity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED;
    }

    private void addGeoFence() {
        if (mGeofenceList != null && mGeofenceList.size() > 0) {
            mGeofenceList.clear();
        }
        mGeofenceList.add(new Geofence.Builder()
                .setRequestId("HHAndroid HQ")
                .setNotificationResponsiveness(1)
                .setCircularRegion(latitude, longitude, GEOFENCE_RADIUS_IN_METERS)
                .setExpirationDuration(GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnFailureListener(e -> {
                    Log.e(TAG, "onFailure: " + e.toString());
                    Toast.makeText(activity, "Failed to add geoFence, " + e.toString(), Toast.LENGTH_SHORT).show();
                })
                .addOnCompleteListener(task -> {
                    Log.d(TAG, "onComplete: GeoFence added status: " + task.isSuccessful());
                    if (task.isSuccessful()) {
                      Calendar c = Calendar.getInstance();
                      geofenceCreateDate = String.valueOf(c.get(Calendar.DATE));
                        //Toast.makeText(activity, "GeoFence Added", Toast.LENGTH_SHORT).show();
                      PrefUtils.saveToPrefs(activity, "geofenceCreateDate", geofenceCreateDate);
                    }
                });
    }

    private void removeGeofence() {
        if (checkPermissions()) {
            mGeofencingClient.removeGeofences(getGeofencePendingIntent()).addOnCompleteListener(task -> {
                Log.d(TAG, "removeGeofences: GeoFence remove status: " + task.isSuccessful());
                PrefUtils.removeFromPrefs(activity, "GEO_STATUS");
//                if (task.isSuccessful())
//                    Toast.makeText(activity, "GeoFence Removed", Toast.LENGTH_SHORT).show();
            });
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        return new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | GeofencingRequest.INITIAL_TRIGGER_DWELL)
                .addGeofences(mGeofenceList)
                .build();
    }

    private PendingIntent getGeofencePendingIntent() {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(activity, GeofenceBroadcastReceiver.class);
        mGeofencePendingIntent = PendingIntent.getBroadcast(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private void exitGeofenceAPI() {
        JSONObject params = new JSONObject();
        try {
            params.put("id", 0);
            params.put("user_id", executeArgs.getString(0));
            params.put("job_id", executeArgs.getString(1));
            params.put("task_code_id", 0);
            params.put("action", "out");
            params.put("time_entry_id", 0);
            params.put("latitude", String.valueOf(latitude));
            params.put("longitude", String.valueOf(longitude));
            params.put("app_version", BuildConfig.VERSION_CODE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(JSON, params.toString());

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Auth-Token", TOKEN)
                .url(URL + "time-entry/save-radius-time-entry/")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "onFailure: api failed" + e.getLocalizedMessage());
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    //response.body() can be consumed only once so use only jsonData
                    String jsonData = response.body().string();
                    Log.d(TAG, "onResponse: " + jsonData);

                    JSONObject jsonObj = new JSONObject(jsonData);
                    String status = jsonObj.getString("status");
                    geofenceCreateDate = (String) PrefUtils.getFromPrefs(activity, "geofenceCreateDate", geofenceCreateDate);
                    Calendar c = Calendar.getInstance();
                    String currentDate = String.valueOf(c.get(Calendar.DATE));
                    if(!currentDate.equals(geofenceCreateDate)){
                      removeGeofence();
                    }
                    if (status != null && status.equals("success")) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void reenterGeofenceAPI() {
      geofenceCreateDate = (String) PrefUtils.getFromPrefs(activity, "geofenceCreateDate", geofenceCreateDate);
      Calendar c = Calendar.getInstance();
      String currentDate = String.valueOf(c.get(Calendar.DATE));
      if(!currentDate.equals(geofenceCreateDate)){
        removeGeofence();
        return;
      }
        JSONObject params = new JSONObject();
        try {
            params.put("id", 0);
            params.put("user_id", executeArgs.getString(0));
            params.put("job_id", executeArgs.getString(1));
            params.put("task_code_id", 0);
            params.put("action", "in");
            params.put("time_entry_id", 0);
            params.put("latitude", String.valueOf(latitude));
            params.put("longitude", String.valueOf(longitude));
            params.put("app_version", BuildConfig.VERSION_CODE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(JSON, params.toString());

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Auth-Token", TOKEN)
                .url(URL + "time-entry/save-radius-time-entry/")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "onFailure: api failed" + e.getLocalizedMessage());
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    //response.body() can be consumed only once so use only jsonData
                    String jsonData = response.body().string();
                    Log.d(TAG, "onResponse: " + jsonData);

                    JSONObject jsonObj = new JSONObject(jsonData);
                    String status = jsonObj.getString("status");
                    if (status != null && status.equals("success")) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }
}
